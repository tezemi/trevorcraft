package com.tryhardtrevor.trevorcraft.entities;

import com.tryhardtrevor.trevorcraft.init.ModItems;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class RedstoneArrowEntity extends EntityArrow
{
    public RedstoneArrowEntity(World worldIn)
    {
        super(worldIn);
    }

    public RedstoneArrowEntity(World worldIn, double x, double y, double z)
    {
        super(worldIn, x, y, z);
    }

    public RedstoneArrowEntity(World worldIn, EntityLivingBase shooter)
    {
        super(worldIn, shooter);
    }

    @Override
    protected ItemStack getArrowStack()
    {
        return new ItemStack(ModItems.REDSTONE_ARROW);
    }
}
