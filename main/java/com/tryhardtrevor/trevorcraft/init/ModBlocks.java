package com.tryhardtrevor.trevorcraft.init;

import java.util.List;
import java.util.ArrayList;
import com.tryhardtrevor.trevorcraft.blocks.*;
import com.tryhardtrevor.trevorcraft.items.Headlamp;
import net.minecraft.block.Block;

public class ModBlocks
{
    public static final List<Block> BLOCKS = new ArrayList<Block>();
    public static final Lift LIFT = new Lift();
    public static final HeadlampLight HEADLAMP_LIGHT = new HeadlampLight();
}
