package com.tryhardtrevor.trevorcraft.init;

import java.util.List;
import java.util.ArrayList;

import com.tryhardtrevor.trevorcraft.util.Reference;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import com.tryhardtrevor.trevorcraft.items.*;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.util.EnumHelper;

public class ModItems
{
    public static final CreativeTabs TREVORCRAFT_TAB = (new CreativeTabs("trevorcraftTab")
    {
        @Override
        public ItemStack getTabIconItem()
        {
            return new ItemStack(ModItems.HOMEWARD_BONE);
        }
    });

    public static final List<Item> ITEMS = new ArrayList<Item>();
    public static final HomewardBone HOMEWARD_BONE = new HomewardBone();
    public static final HomewardIdol HOMEWARD_IDOL = new HomewardIdol();
    public static final RedstoneArrow REDSTONE_ARROW = new RedstoneArrow();

    public static final ItemArmor.ArmorMaterial HEADLAMP_MATERIAL = EnumHelper.addArmorMaterial
    (
        "armor_material_headlamp",
        Reference.MOD_ID + ":headlamp",
        14,
        new int [] {2, 7, 5, 3},
        10,
        SoundEvents.ITEM_ARMOR_EQUIP_LEATHER,
        0.0f
    );

    public static final Headlamp HEADLAMP = new Headlamp(HEADLAMP_MATERIAL, 1, EntityEquipmentSlot.HEAD);
}
