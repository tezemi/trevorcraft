package com.tryhardtrevor.trevorcraft;

import com.tryhardtrevor.trevorcraft.items.Headlamp;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.Mod;

public class EventHandlers
{
    @Mod.EventHandler
    public void onLivingUpdateEvent(LivingEvent.LivingUpdateEvent event)
    {
        Entity ent = event.getEntity();
        if (ent instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer)ent;
            ItemStack heldItem = player.getHeldItem(EnumHand.MAIN_HAND);
            if (heldItem != null && heldItem.getItem() instanceof Headlamp)
            {
                Minecraft.getMinecraft().player.sendChatMessage("Player is holding headlamp");
            }
        }
    }
}
