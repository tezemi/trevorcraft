package com.tryhardtrevor.trevorcraft.tileentities;

import com.tryhardtrevor.trevorcraft.blocks.Lift;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.tileentity.TileEntity;
import com.tryhardtrevor.trevorcraft.util.Reference;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.List;

public class LiftTile extends TileEntity implements ITickable
{
    public static final double CART_EXIT_TOP_VELOCITY = 0.25d;
    protected IBlockState StateOfLiftBlock;
    private int _lastDirection;

    public LiftTile()
    {
        GameRegistry.registerTileEntity(this.getClass(), new ResourceLocation(Reference.MOD_ID + ":headlamp"));
    }

    public void setStateOfLiftBlock(IBlockState state)
    {
        StateOfLiftBlock = state;
    }

    @Override
    public void update()
    {
        AxisAlignedBB scanAround = new AxisAlignedBB(getPos().getX() - 1, pos.getY() - 1, pos.getZ() - 1, pos.getX() + 2, pos.getY() + 2, pos.getZ() + 2);
        List entities = world.getEntitiesWithinAABB(EntityMinecart.class, scanAround);
        for (Object e : entities)
        {
            if (e instanceof EntityMinecart)
            {
                EntityMinecart cart = (EntityMinecart)e;
                Minecraft.getMinecraft().player.sendChatMessage("Lifting a cart!");
                BlockPos cartPos = cart.getPosition();
                BlockPos behind = new BlockPos(cartPos.getX() - 1, cartPos.getY(), cartPos.getZ());
                BlockPos front = new BlockPos(cartPos.getX() + 1, cartPos.getY() + 1, cartPos.getZ());
                BlockPos left = new BlockPos(cartPos.getX(), cartPos.getY(), cartPos.getZ() - 1);
                BlockPos right = new BlockPos(cartPos.getX(), cartPos.getY() + 1, cartPos.getZ() + 1);
                if (cart.world.getBlockState(behind) == StateOfLiftBlock || cart.world.getBlockState(front) == StateOfLiftBlock ||
                cart.world.getBlockState(left) == StateOfLiftBlock || cart.world.getBlockState(right) == StateOfLiftBlock)
                {
                    if (cart.motionY >= 0d) // Cart is not moving upward or is moving upward and cart is below block
                    {
                        if (cart.world.getBlockState(behind) == StateOfLiftBlock)
                        {
                            _lastDirection = 0;
                        }
                        else if (cart.world.getBlockState(front) == StateOfLiftBlock)
                        {
                            _lastDirection = 1;
                        }
                        else if (cart.world.getBlockState(left) == StateOfLiftBlock)
                        {
                            _lastDirection = 2;
                        }
                        else if (cart.world.getBlockState(right) == StateOfLiftBlock)
                        {
                            _lastDirection = 3;
                        }

                        cart.setCanUseRail(false);
                        cart.setVelocity(0d, 0.25d, 0d);
                        Minecraft.getMinecraft().player.sendChatMessage("Adding vel to cart!");
                    }
                    else if (cart.motionY < 0 || pos.getY() < cart.getPosition().getY()) // Cart is moving down or cart is above block
                    {
                        cart.setCanUseRail(false);
                        cart.setVelocity(0d, -0.25d, 0d);
                        Minecraft.getMinecraft().player.sendChatMessage("Adding neg val to cart!");
                    }
                }
                else if (cart.motionY > 0d)     // Don't want to affect the cart's movement unless it's being moved upwards by the lift
                {
                    cart.setVelocity
                    (
                        _lastDirection == 0 ? -CART_EXIT_TOP_VELOCITY : _lastDirection == 1 ? CART_EXIT_TOP_VELOCITY : 0d,
                        CART_EXIT_TOP_VELOCITY,
                        _lastDirection == 2 ? -CART_EXIT_TOP_VELOCITY : _lastDirection == 3 ? CART_EXIT_TOP_VELOCITY : 0d
                    );

                    cart.setCanUseRail(true);
                    Minecraft.getMinecraft().player.sendChatMessage("Cart off rails...");
                }
            }
        }
    }
}
