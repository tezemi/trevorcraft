package com.tryhardtrevor.trevorcraft.tileentities;

import java.util.List;
import java.util.ArrayList;
import net.minecraft.tileentity.TileEntity;

public class ModTiles
{
    public static final List<TileEntity> TILES = new ArrayList<TileEntity>();
    public static final LiftTile LIFT_TILE = new LiftTile();
}
