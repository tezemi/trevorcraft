package com.tryhardtrevor.trevorcraft.items;

import com.tryhardtrevor.trevorcraft.Main;
import com.tryhardtrevor.trevorcraft.util.Helpers;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.util.IHasModel;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.block.BlockPortal;
import net.minecraft.block.BlockTorch;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.item.ItemBow;
import net.minecraft.potion.Potion;
import net.minecraft.util.*;
import net.minecraft.item.Item;
import net.minecraft.init.Items;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.init.SoundEvents;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Item that teleports the player home when used, but the item has infinite uses.
 * The item vanishes if the player drops it, even upon death.
 */
public class HomewardIdol extends Item implements IHasModel, IHasRecipe
{
    public HomewardIdol()
    {
        setUnlocalizedName("homeward_idol");
        setRegistryName("homeward_idol");
        setCreativeTab(ModItems.TREVORCRAFT_TAB);
        ModItems.ITEMS.add(this);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        world.playSound(player.getPosition().getX(),  player.getPosition().getY(), player.getPosition().getZ(), SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS, 1f, 1f, false);
        Helpers.CreateParticleCloud(world, EnumParticleTypes.PORTAL, player.getPosition(), 12, 10);
        Helpers.TeleportPlayerToSpawn(player);
        world.playSound(player.getPosition().getX(),  player.getPosition().getY(), player.getPosition().getZ(), SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS, 1f, 1f, false);
        Helpers.CreateParticleCloud(world, EnumParticleTypes.PORTAL, player.getPosition(), 12, 10);

        return new ActionResult<>(EnumActionResult.PASS, player.getHeldItem(hand));
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player)
    {
        item.setCount(0);       // Item vanishes upon drop

        return true;
    }

    @Override
    public int getEntityLifespan(ItemStack itemStack, World world)
    {
        itemStack.setCount(0);

        return 0;               // Item vanishes upon drop
    }

    @Override
    public void registerModel()
    {
        Main.proxy.registerItemRenderer(this, 0, "inventory");
    }

    @Override
    public boolean isFull3D()
    {
        return false;
    }

    @Override
    public void registerRecipe(ResourceLocation group)
    {
        GameRegistry.addShapedRecipe(new ResourceLocation("trevorcraft:tcm_recipe_homeward_idol"), group, new ItemStack(ModItems.HOMEWARD_IDOL), new Object[]
        {
            "GDG",
            "SSS",
            " S ",
            'D', Items.DIAMOND,
            'S', Blocks.COBBLESTONE,
            'G', Items.ENDER_PEARL
        });
    }
}
