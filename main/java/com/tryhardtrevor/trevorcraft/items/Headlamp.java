package com.tryhardtrevor.trevorcraft.items;

import com.tryhardtrevor.trevorcraft.Main;
import com.tryhardtrevor.trevorcraft.init.ModBlocks;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.util.IHasModel;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.util.*;
import net.minecraft.item.Item;
import net.minecraft.init.Items;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.List;

/**
 * Leather helmet with a light on the front. The player emits light
 * when they wear this. The durability degrades over time.
 */
public class Headlamp extends ItemArmor implements IHasModel, IHasRecipe
{
    private BlockPos _previousBlockPos;

    public Headlamp(ArmorMaterial armorMaterial, int renderIndex, EntityEquipmentSlot slot)
    {
        super(armorMaterial, renderIndex, slot);
        setUnlocalizedName("headlamp");
        setRegistryName("headlamp");
        setCreativeTab(ModItems.TREVORCRAFT_TAB);
        ModItems.ITEMS.add(this);
    }

    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack)
    {
        if (world.getBlockState(player.getPosition()) == Blocks.AIR.getDefaultState())
        {
            IBlockState lightBlock = ModBlocks.HEADLAMP_LIGHT.getDefaultState();
            world.setBlockState(player.getPosition(), lightBlock);
        }

        if (_previousBlockPos != null && _previousBlockPos != player.getPosition() && world.getBlockState(_previousBlockPos) == ModBlocks.HEADLAMP_LIGHT.getDefaultState())
        {
            world.setBlockState(_previousBlockPos, Blocks.AIR.getDefaultState());
        }
    }

    @Override
    public void registerModel()
    {
        Main.proxy.registerItemRenderer(this, 0, "inventory");
    }

    @Override
    public boolean isFull3D()
    {
        return false;
    }

    @Override
    public void registerRecipe(ResourceLocation group)
    {
        GameRegistry.addShapedRecipe(new ResourceLocation("trevorcraft:tcm_recipe_headlamp"), group, new ItemStack(ModItems.HEADLAMP), new Object[]
        {
            " G ",
            "GSG",
            " L ",
            'L', Items.LEATHER_HELMET,
            'G', Blocks.GLASS,
            'S', Blocks.GLOWSTONE
        });
    }
}
