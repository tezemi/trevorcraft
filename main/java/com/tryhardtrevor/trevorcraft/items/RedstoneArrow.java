package com.tryhardtrevor.trevorcraft.items;

import com.tryhardtrevor.trevorcraft.entities.RedstoneArrowEntity;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Items;
import net.minecraft.util.*;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Item that teleports the player home when used, which also consumes the item.
 */
public class RedstoneArrow extends Item implements IHasRecipe
{
    public RedstoneArrow()
    {
        setUnlocalizedName("redstone_arrow");
        setRegistryName("redstone_arrow");
        setCreativeTab(ModItems.TREVORCRAFT_TAB);
        ModItems.ITEMS.add(this);
    }

    public EntityArrow createArrow(World worldIn, ItemStack stack, EntityLivingBase shooter)
    {
        RedstoneArrowEntity ent = new RedstoneArrowEntity(worldIn, shooter);

        return ent;
    }

    public boolean isInfinite(ItemStack stack, ItemStack bow, net.minecraft.entity.player.EntityPlayer player)
    {
        int enchant = net.minecraft.enchantment.EnchantmentHelper.getEnchantmentLevel(net.minecraft.init.Enchantments.INFINITY, bow);
        return enchant <= 0 ? false : this.getClass() == RedstoneArrow.class;
    }

    @Override
    public void registerRecipe(ResourceLocation group)
    {
        GameRegistry.addShapedRecipe(new ResourceLocation("trevorcraft:tcm_recipe_redstone_arrow"), group, new ItemStack(ModItems.REDSTONE_ARROW), new Object[]
        {
            " R ",
            " A ",
            " I ",
            'R', Items.REDSTONE,
            'A', Items.ARROW,
            'I', Items.IRON_INGOT
        });
    }
}
