package com.tryhardtrevor.trevorcraft.items;

import com.tryhardtrevor.trevorcraft.Main;
import com.tryhardtrevor.trevorcraft.util.Helpers;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.util.IHasModel;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.init.Items;
import net.minecraft.util.*;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.init.SoundEvents;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Item that teleports the player home when used, which also consumes the item.
 */
public class HomewardBone extends Item implements IHasModel, IHasRecipe
{
    public HomewardBone()
    {
        setUnlocalizedName("homeward_bone");
        setRegistryName("homeward_bone");
        setCreativeTab(ModItems.TREVORCRAFT_TAB);
        ModItems.ITEMS.add(this);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        world.playSound(player.getPosition().getX(),  player.getPosition().getY(), player.getPosition().getZ(), SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS, 1f, 1f, false);
        Helpers.CreateParticleCloud(world, EnumParticleTypes.PORTAL, player.getPosition(), 12, 10);
        Helpers.TeleportPlayerToSpawn(player);
        world.playSound(player.getPosition().getX(),  player.getPosition().getY(), player.getPosition().getZ(), SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS, 1f, 1f, false);
        Helpers.CreateParticleCloud(world, EnumParticleTypes.PORTAL, player.getPosition(), 12, 10);
        ItemStack stack = player.getHeldItem(hand);
        stack.setCount(stack.getCount() - 1);

        return new ActionResult<>(EnumActionResult.PASS, player.getHeldItem(hand));
    }

    @Override
    public void registerModel()
    {
        Main.proxy.registerItemRenderer(this, 0, "inventory");
    }

    @Override
    public boolean isFull3D()
    {
        return false;
    }

    @Override
    public void registerRecipe(ResourceLocation group)
    {
        GameRegistry.addShapedRecipe(new ResourceLocation("trevorcraft:tcm_recipe_homeward_bone"), group, new ItemStack(ModItems.HOMEWARD_BONE), new Object[]
        {
            "   ",
            "EBE",
            "   ",
            'B', Items.BONE,
            'E', Items.ENDER_PEARL
        });
    }
}
