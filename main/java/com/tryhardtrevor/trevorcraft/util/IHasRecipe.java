package com.tryhardtrevor.trevorcraft.util;

import net.minecraft.util.ResourceLocation;

public interface IHasRecipe
{
    void registerRecipe(ResourceLocation group);
}
