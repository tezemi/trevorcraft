package com.tryhardtrevor.trevorcraft.util;

import java.util.Random;

import ibxm.Player;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumParticleTypes;

public class Helpers
{
    public static void CreateParticleCloud(World world, EnumParticleTypes type, BlockPos pos, int cloudSize, int cloudSpread)
    {
        for (int i = 0; i < cloudSize; i++)
        {
            Random rand = new Random();
            world.spawnParticle(type, pos.getX(), pos.getY(), pos.getZ(), rand.nextInt(cloudSpread) + 2, rand.nextInt(cloudSpread) - cloudSpread, rand.nextInt(cloudSpread) - cloudSpread, 0);
        }
    }

    public static void TeleportPlayerToSpawn(EntityPlayer player)
    {
        // Get new position
        double newX;
        double newY;
        double newZ;
        if (player.getBedLocation() != null)        // If the player has a bed...
        {
            newX = player.getBedLocation().getX();  // Use that as their spawn (because it is)
            newY = player.getBedLocation().getY();
            newZ = player.getBedLocation().getZ();
        }
        else
        {
            newX = player.getEntityWorld().getSpawnPoint().getX();  // If not, than it's the default spawn point in the world
            newY = player.getEntityWorld().getSpawnPoint().getY();
            newZ = player.getEntityWorld().getSpawnPoint().getZ();
        }

        // Try to find Y location that has no blocks. Sometimes spawn points start in the ground
        while (player.world.getBlockState(new BlockPos(newX, newY, newZ)) != Blocks.AIR.getDefaultState() && newY < 254d)
        {
            newY += 1d;
        }

        player.setPositionAndUpdate(newX, newY, newZ);
    }
}
