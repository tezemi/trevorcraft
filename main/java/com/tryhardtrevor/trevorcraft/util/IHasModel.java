package com.tryhardtrevor.trevorcraft.util;

public interface IHasModel
{
    void registerModel();
}
