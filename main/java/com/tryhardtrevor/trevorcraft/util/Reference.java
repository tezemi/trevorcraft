package com.tryhardtrevor.trevorcraft.util;

public class Reference
{
    public static final String MOD_ID = "tcm";
    public static final String NAME = "Trevor Craft";
    public static final String VERSION = "1.0";
    public static final String CLIENT_PROXY_CLASS = "com.tryhardtrevor.trevorcraft.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.tryhardtrevor.trevorcraft.proxy.CommonProxy";
}
