package com.tryhardtrevor.trevorcraft.blocks;

import com.tryhardtrevor.trevorcraft.Main;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.tileentities.LiftTile;
import com.tryhardtrevor.trevorcraft.util.IHasModel;
import com.tryhardtrevor.trevorcraft.init.ModBlocks;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.init.Items;
import net.minecraft.init.Blocks;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.List;
import java.util.Random;

public class Lift extends Block implements IHasModel, IHasRecipe
{
    public Lift()
    {
        super(Material.IRON);
        setRegistryName("lift");
        setUnlocalizedName("lift");
        setCreativeTab(ModItems.TREVORCRAFT_TAB);
        setHardness(5);
        setHarvestLevel("pickaxe", 1);
        setTickRandomly(true);
        ModBlocks.BLOCKS.add(this);                                                         // Adds to collection of all blocks
        ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));    // Adds the block as an item
    }

    @Override
    public void registerModel()
    {
        Main.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        if (hasTileEntity(state))
        {
            LiftTile liftTile = new LiftTile();
            liftTile.setStateOfLiftBlock(state);
            return liftTile;
        }

        return null;
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.SOLID;          // Defines block physics
    }

    @Override
    public void registerRecipe(ResourceLocation group)
    {
        GameRegistry.addShapedRecipe(new ResourceLocation("trevorcraft:tcm_recipe_lift"), group, new ItemStack(ModBlocks.LIFT, 3), new Object[]
        {
            "WRW",
            "WIW",
            "WRW",
            'I', Blocks.IRON_BLOCK,
            'R', Items.REDSTONE,
            'W', Blocks.PLANKS
        });
    }
}