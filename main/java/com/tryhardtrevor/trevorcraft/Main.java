package com.tryhardtrevor.trevorcraft;

import com.tryhardtrevor.trevorcraft.init.ModBlocks;
import com.tryhardtrevor.trevorcraft.init.ModItems;
import com.tryhardtrevor.trevorcraft.util.IHasRecipe;
import net.minecraft.item.Item;
import net.minecraft.block.Block;
import net.minecraftforge.fml.common.Mod;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.Mod.Instance;
import com.tryhardtrevor.trevorcraft.util.Reference;
import com.tryhardtrevor.trevorcraft.proxy.CommonProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;

@Mod(modid = Reference.MOD_ID, name = Reference.NAME, version = Reference.VERSION)
public class Main
{
    @Instance
    public static Main instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.COMMON_PROXY_CLASS)
    public static CommonProxy proxy;

    public static void preInitClientOnly()
    {

    }

    @Mod.EventHandler
    public static void PreInit(FMLPreInitializationEvent event)
    {

    }

    @Mod.EventHandler
    public static void init(FMLInitializationEvent event)
    {
        ResourceLocation optionalGroup = new ResourceLocation("");

        // Registers recipes with all items that implement IHasRecipe
        for (Item i : ModItems.ITEMS)
        {
            if (i instanceof IHasRecipe)
            {
                ((IHasRecipe)i).registerRecipe(optionalGroup);
            }
        }

        // Same for block recipes
        for (Block b : ModBlocks.BLOCKS)
        {
            if (b instanceof IHasRecipe)
            {
                ((IHasRecipe)b).registerRecipe(optionalGroup);
            }
        }
    }

    @Mod.EventHandler
    public static void Postinit(FMLPostInitializationEvent event)
    {

    }
}
